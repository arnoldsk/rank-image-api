<?php

//error_reporting(-1);
//ini_set('display_errors', 'On');

/**
 * Functions
 */
function map($value, $fromLow, $fromHigh, $toLow, $toHigh) {
    $fromRange = $fromHigh - $fromLow;
    $toRange = $toHigh - $toLow;
    $scaleFactor = $toRange / $fromRange;

    // Re-zero the value within the from range
    $tmpValue = $value - $fromLow;
    // Rescale the value to the to range
    $tmpValue *= $scaleFactor;
    // Re-zero back to the to range
    return $tmpValue + $toLow;
}

function param($key, $fallback = null) {
    return isset($_GET[$key]) && strlen($_GET[$key]) ? $_GET[$key] : $fallback;
}

function formatNumber($number) {
    $trillion = 1000000000000;
    $billion = 1000000000;
    $million = 1000000;
    $thousand = 1000;

    if ($number >= $trillion) {
        $number = round($number / $trillion, 2) . ' T';
    } else if ($number >= $billion) {
        $number = round($number / $billion, 2) . ' B';
    } else if ($number >= $million) {
        $number = round($number / $million, 2) . ' M';
    } else if ($number >= $thousand) {
        $number = round($number / $thousand, 2) . ' K';
    }

    return $number;
}

function getResizedGif(Imagick $image, $width) {
    $image = $image->coalesceImages();

    foreach ($image as $frame) {
        $frame->thumbnailImage($width, 0);
        $frame->cropImage($width, $width, 0, 0);
        $frame->setImagePage($width, $width, 0, 0);
    }

    return $image->deconstructImages();
}

function getPngFromGif($image) {
    if ($image->getImageType() === Imagick::INTERLACE_GIF) {
        $png = new Imagick();
        $png->setFormat('png');

        foreach ($image as $frame) {
            $png->addImage($frame->getImage());
            break;
        }

        return $png;
    }

    return $image;
}

function verifyImage($image) {
    $size = @getimagesize($image);
    $mimes = [
        'image/gif', 'image/jpeg', 'image/png',
    ];

    return (
        // Is an array - a successful size fetch
        is_array($size)
        // Basically check for necessary values
        && count($size) >= 2
        && isset($size['mime'])
        // Correct mime type
        && in_array($size['mime'], $mimes)
        // Less than 4K w/h
        && $size[0] <= 3840
        && $size[1] <= 3840
    );
}


/**
 * Dynamic variables
 */
$defaultAvatar = 'assets/transparent.png';
$data = [
    'name' => param('name', 'Unknown'),
    'avatarUrl' => param('avatar_url', $defaultAvatar),
    'level' => param('level', 0),
    'expCurrent' => (int)param('exp_current', 0),
    'expMinimum' => (int)param('exp_minimum', 0),
    'expMaximum' => (int)param('exp_maximum', 1),
];

extract($data);

// Validate exp values
if ($expMinimum < 0) $expMinimum = 0;
if ($expMaximum <= $expMinimum) $expMaximum = $expMinimum + 1;
if ($expMaximum < 1) $expMinimum = 1;

/**
 * Image variables
 */
$width = 225;
$height = 50;
$fontPath = __DIR__ . '/assets/fonts/seguiemj.ttf';
$backgroundPath = __DIR__ . '/assets/background.jpg';
$lightColor = '#fff';
$darkColor = '#222';


/**
 * Background base image
 */
$image = new Imagick();
$image->readImage($backgroundPath);
$image = getPngFromGif($image);
$image->scaleImage($width, 0);
$image->cropImage($width, $height, 0, 0);


/**
 * Name
 */
$nameFont = new ImagickDraw();
$nameFont->setFont($fontPath);
$nameFont->setFontSize(21);
$nameFont->setFontWeight(800);
$nameFont->setFillColor($lightColor);

// Get height offset
$nameFontMetrics = $image->queryFontMetrics($nameFont, $name);
$nameFontY = $nameFontMetrics['ascender'];

// Draw
$nameFont->annotation($height, $nameFontY + 5, $name);
$image->drawImage($nameFont);


/**
 * "Level"
 */
$levelFont = new ImagickDraw();
$levelFont->setFont($fontPath);
$levelFont->setFontSize(14);
$levelFont->setFillColor($lightColor);

// Get height offset
$levelFontMetrics = $image->queryFontMetrics($levelFont, 'LVL ' . $level);
$levelFontY = $levelFontMetrics['ascender'];
$levelFontX2 = $height + 5 + $levelFontMetrics['textWidth'];

// Draw
$levelFont->annotation($height, $height - 14, 'LVL ' . $level);
$image->drawImage($levelFont);


/**
 * Exp bar background
 */
$expBarBg = new ImagickDraw();
$expBarBg->setFillColor("#000000");
$expBarBg->rectangle($height, $height - 9, $width - 10, $height - 7.5);

// Draw
$image->drawImage($expBarBg);


/**
 * Current exp bar
 */
$expBar = new ImagickDraw();
$expBar->setFillColor($lightColor);
$expBar->setFillOpacity(0.5);

$expBarGap = 0;
$expBarXOffset = map($expCurrent, $expMinimum, $expMaximum, $height + 5 + $expBarGap, $width - 10 - $expBarGap);
if ($expBarXOffset > $width - 10 - $expBarGap) {
    $expBarXOffset = $width - 10 - $expBarGap;
}

$expBar->rectangle(
    $height + $expBarGap,
    $height - 9 + $expBarGap,
    $expBarXOffset,
    $height - 7.5 - $expBarGap
);

// Draw
$image->drawImage($expBar);


/**
 * Exp out of max text
 */
$font = new ImagickDraw();
$font->setFont($fontPath);
$font->setFontSize(10);
$font->setFontWeight(800);
$font->setFillOpacity(0.5);
$font->setFillColor($lightColor);

// Get height offset
$metrics = $image->queryFontMetrics($font, formatNumber($expCurrent) . ' / ' . formatNumber($expMaximum));
$y = $metrics['ascender'];

// Draw
$font->annotation(
    $levelFontX2 + 5,
    $height - 14,
    formatNumber($expCurrent) . ' / ' . formatNumber($expMaximum)
);
$image->drawImage($font);

/**
 * Avatar
 */
$tmp = @tempnam('cache/images', 'gif_');
if (@copy($avatarUrl, $tmp)) {
    $avatar = new Imagick($tmp);
} else {
    $avatar = new Imagick(__DIR__ . '/assets/transparent.png');
}

/**
 * RENDER
 * We are implying that the avatar is a GIF by default
 */
$gifFrames = $avatar->coalesceImages();

// For large avatars reduce the frame count
if (count($gifFrames) > 150) {
    $gifFrames = [getPngFromGif($avatar)];
}

$outputGif = new Imagick();
$outputGif->setFormat('gif');

foreach ($gifFrames as $avatarFrame) {
    $frame = clone $image;

    // Add the avatar frame
    $avatarFrame->scaleImage(0, $height - 10);
    // Center crop
    $avatarFrame->cropImage($height, $height - 10, ($avatarFrame->getImageWidth() - $height) / 2 - 5, 0);
    $frame->compositeimage($avatarFrame, Imagick::COMPOSITE_OVER, 5, 5);

    // Fix framerate
    $frame->setImageDelay($avatar->getImageDelay());

    // Tris aligns the positioning of all childs, I think...
    $frame->setImagePage($width, $height, 0, 0);

    $outputGif->addImage($frame);
}

header('Content-type: image/gif');
echo $outputGif->getImagesBlob();

$avatar->destroy();
$outputGif->destroy();
